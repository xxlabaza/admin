/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin;

import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 02.04.2016
 */
@Configuration
class AppConfiguration {

    @Bean
    @ConfigurationPropertiesBinding
    StringToPathConverter stringToPathConverter () {
        return new StringToPathConverter();
    }

    static class StringToPathConverter implements Converter<String, Path> {

        @Override
        public Path convert (String source) {
            return Paths.get(source);
        }
    }
}
