/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin;

import java.nio.file.Path;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 02.04.2016
 */
@Data
@Component
@ConfigurationProperties(prefix = "app")
public class AppProperties {

    private Storage storage;

    private Server server;

    @Data
    public static class Storage {

        private Path home;

        private String keyFileExtension;
    }

    @Data
    public static class Server {

        private String url;

        private String adminPath;

        private Security security;

        @Data
        public static class Security {

            private String headerName;
        }
    }
}
