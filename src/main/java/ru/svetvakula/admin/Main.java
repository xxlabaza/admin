/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.val;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import ru.svetvakula.admin.gui.FXMLLoaderService;
import ru.svetvakula.admin.gui.authentication.AuthenticationController;
import ru.svetvakula.admin.gui.dashboard.DashboardController;
import ru.svetvakula.admin.service.authentication.AuthenticationService;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 01.04.2016
 */
@SpringBootApplication
public class Main extends Application {

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main (String[] args) {
        launch(args);
    }

    private ConfigurableApplicationContext context;

    @Override
    public void start (Stage stage) throws Exception {
        val args = getParameters().getRaw().stream().toArray(String[]::new);
        context = SpringApplication.run(Main.class, args);

        val loader = context.getBean(FXMLLoaderService.class);
        val authenticationService = context.getBean(AuthenticationService.class);

        Parent root;
        if (authenticationService.isAuthenticated()) {
            root = loader.loadNode(DashboardController.FXML, DashboardController.LOCALIZATION);
            root.getStylesheets().add(getClass().getResource("/style/dashboard.css").toExternalForm());
        } else {
            root = loader.loadNode(AuthenticationController.FXML, AuthenticationController.LOCALIZATION);
        }

        val scene = new Scene(root);

        stage.setTitle("Admin Dashboard");
        stage.setResizable(true);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setOnCloseRequest(event -> {
            Platform.exit();
            System.exit(0);
        });

        stage.show();
    }

    @Override
    public void stop () throws Exception {
        super.stop();
        if (context.isActive()) {
            context.close();
        }
    }
}
