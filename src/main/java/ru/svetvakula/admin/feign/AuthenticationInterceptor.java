/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.feign;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.svetvakula.admin.AppProperties;
import ru.svetvakula.admin.service.authentication.AuthenticationService;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 02.04.2016
 */
@Component
class AuthenticationInterceptor implements RequestInterceptor {

    @Autowired
    private AuthenticationService authenticationService;

    private final String serverPath;

    private final String headerName;

    @Autowired
    AuthenticationInterceptor (AppProperties appProperties) {
        val server = appProperties.getServer();
        serverPath = server.getAdminPath();
        headerName = server.getSecurity().getHeaderName();
    }

    @Override
    public void apply (RequestTemplate template) {
        if (!template.url().startsWith(serverPath) || hasAuthenticationHeader(template)) {
            return;
        }

        authenticationService.getUid().ifPresent(key -> {
            template.header(headerName, key);
        });
    }

    private boolean hasAuthenticationHeader (RequestTemplate template) {
        val headers = template.headers();
        return headers != null && headers.containsKey(headerName);
    }
}
