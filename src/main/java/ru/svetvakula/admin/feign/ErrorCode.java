/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.feign;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 04.04.2016
 */
@Target(TYPE)
@Retention(RUNTIME)
public @interface ErrorCode {

    int value ();

    String reason () default "";
}
