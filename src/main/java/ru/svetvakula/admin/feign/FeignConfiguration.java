/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.feign;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import feign.codec.Encoder;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.TimeZone;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.xxlabaza.feign.form.FormEncoder;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.PUBLIC_ONLY;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 05.04.2016
 */
@Configuration
class FeignConfiguration {

    @Value("${app.formatter.date}")
    private String dateFormatter;

    @Value("${app.formatter.time}")
    private String timeFormatter;

    @Value("${app.formatter.date-time}")
    private String dateTimeFormatter;

    @Value("${app.formatter.zoned-date-time}")
    private String zonedDateTimeFormatter;

    @Bean
    Encoder encoder (ObjectMapper objectMapper) {
        return new FormEncoder(new JacksonEncoder(objectMapper));
    }

    @Bean
    JacksonDecoder jacksonDecoder (ObjectMapper objectMapper) {
        return new JacksonDecoder(objectMapper);
    }

    @Bean
    DateTimeFormatter dateFormatter () {
        return DateTimeFormatter.ofPattern(dateFormatter);
    }

    @Bean
    DateTimeFormatter timeFormatter () {
        return DateTimeFormatter.ofPattern(timeFormatter);
    }

    @Bean
    DateTimeFormatter dateTimeFormatter () {
        return DateTimeFormatter.ofPattern(dateTimeFormatter);
    }

    @Bean
    DateTimeFormatter zonedDateTimeFormatter () {
        return DateTimeFormatter.ofPattern(zonedDateTimeFormatter);
    }

    @Bean
    ObjectMapper objectMapper () {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setDateFormat(new SimpleDateFormat(dateFormatter));
        objectMapper.setLocale(Locale.getDefault());
        objectMapper.setTimeZone(TimeZone.getDefault());
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(zonedDateTimeFormatter()));
        objectMapper.registerModule(javaTimeModule);
        objectMapper.configure(WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.setVisibility(objectMapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(NONE)
                .withGetterVisibility(PUBLIC_ONLY)
                .withSetterVisibility(PUBLIC_ONLY)
                .withCreatorVisibility(NONE)
        );
        return objectMapper;
    }
}
