/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.feign;

import ru.svetvakula.admin.feign.exception.UnableToFindFeignClientError;
import ru.svetvakula.admin.feign.exception.UnknownRemoteApiException;
import ru.svetvakula.admin.feign.exception.UnauthorizedException;
import ru.svetvakula.admin.feign.exception.PermissionDeniedException;
import feign.Response;
import feign.codec.ErrorDecoder;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.stream.Stream;
import lombok.val;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 04.04.2016
 */
@Component
public class FeignErrorDecoder implements ErrorDecoder {

    private final static String REASON_HEADER_NAME;

    static {
        REASON_HEADER_NAME = "X-Status-Reason";
    }

    @Autowired
    private ApplicationContext context;

    @Override
    public Exception decode (String methodKey, Response response) {
        switch (response.status()) {
        case 401:
            return new UnauthorizedException();
        case 403:
            return new PermissionDeniedException();
        }

        val exceptionTypeArrays = getMethodExceptions(methodKey);
        Stream<Class<?>> stream = Stream.of(exceptionTypeArrays)
                .filter(Exception.class::isAssignableFrom)
                .filter(it -> it.isAnnotationPresent(ErrorCode.class))
                .filter(it -> it.getAnnotation(ErrorCode.class).value() == response.status());

        if (response.headers().containsKey(REASON_HEADER_NAME)) {
            String reason = response.headers().get(REASON_HEADER_NAME).iterator().next();
            stream.filter(it -> it.getAnnotation(ErrorCode.class).reason().equals(reason));
        }
        return stream
                .findFirst()
                .map(it -> BeanUtils.instantiateClass(it, Exception.class))
                .orElse(new UnknownRemoteApiException());
    }

    private Class<?>[] getMethodExceptions (String methodKey) {
        // AuthenticationApi#authenticate(String)
        val methodKeyTokens = methodKey.split("#");
        val methodSignature = methodKeyTokens[1];
        val methodName = methodSignature.substring(0, methodSignature.lastIndexOf('('));
        val argumentTypeNames = methodSignature.substring(
                methodSignature.indexOf('(') + 1, methodSignature.indexOf(')')
        ).split(",");

        return context.getBeansOfType(Object.class).values().stream()
                .map(Object::getClass)
                .filter(Proxy::isProxyClass)
                .map(Class::getInterfaces)
                .flatMap(Stream::of)
                .filter(it -> it.getSimpleName().equals(methodKeyTokens[0]))
                .map(Class::getMethods)
                .flatMap(Stream::of)
                .filter(it -> it.getName().equals(methodName))
                .filter(it -> it.getParameterCount() == argumentTypeNames.length)
                .filter(it -> {
                    String[] methodArgumentTypes = Stream.of(it.getParameterTypes())
                            .map(Class::getSimpleName)
                            .toArray(String[]::new);
                    return Arrays.equals(argumentTypeNames, methodArgumentTypes);
                })
                .findFirst()
                .map(Method::getExceptionTypes)
                .orElseThrow(UnableToFindFeignClientError::new);
    }
}
