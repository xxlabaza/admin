/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.feign;

import java.io.Serializable;
import lombok.Data;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Data
public class Filter implements Serializable {

    private static final long serialVersionUID = 7626405896919886854L;

    private String filed;

    private Operator operator;

    private String value;

    public Filter () {
    }

    public Filter (String filed, Operator operator, String value) {
        this.filed = filed;
        this.operator = operator;
        this.value = value;
    }

    @Override
    public String toString () {
        return new StringBuilder()
                .append(filed)
                .append(' ')
                .append(operator.getSymbol())
                .append(' ')
                .append(value)
                .toString();
    }

    public enum Operator {

        EQUALS("="),
        NOT_EQUALS("!="),
        GREATER_THAN(">"),
        GREATER_THAN_EQUALS(">="),
        LESS_THAN("<"),
        LESS_THAN_EQUALS("<="),
        CONTAINS("has"),
        STARTS_WITH("starts"),
        ENDS_WITH("ends");

        private final String symbol;

        private Operator (String symbol) {
            this.symbol = symbol;
        }

        public String getSymbol () {
            return symbol;
        }
    }
}
