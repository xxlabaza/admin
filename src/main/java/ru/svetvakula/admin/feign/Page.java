/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.feign;

import java.io.Serializable;
import java.util.List;
import lombok.Data;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Data
public class Page<T> implements Serializable {

    private List<String> filters;

    private List<String> sorts;

    private List<T> content;

    private int page;

    private int size;

    private int elements;

    private int total;

    private boolean hasNext;

    private boolean hasPrevious;

    private int pages;
}
