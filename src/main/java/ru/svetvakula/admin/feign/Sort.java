/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.feign;

import java.io.Serializable;
import lombok.Data;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Data
public class Sort implements Serializable {

    private static final long serialVersionUID = 3320845056079942860L;

    private String field;

    private Direction direction;

    @Override
    public String toString () {
        return new StringBuilder()
                .append(field)
                .append(' ')
                .append(direction.name())
                .toString();
    }

    public enum Direction {

        ASC,
        DESC;
    }
}
