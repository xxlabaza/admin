/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.feign.exception;

import ru.svetvakula.admin.feign.ErrorCode;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 05.04.2016
 */
@ErrorCode(403)
public class PermissionDeniedException extends RuntimeException {

    private static final long serialVersionUID = 4508862451005147316L;

}
