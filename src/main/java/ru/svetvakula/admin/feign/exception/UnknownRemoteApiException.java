/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.feign.exception;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 04.04.2016
 */
public class UnknownRemoteApiException extends RuntimeException {

    private static final long serialVersionUID = 7658483981669060207L;

}
