/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui;

import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.util.Callback;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 01.04.2016
 */
@Service
public class FXMLLoaderService {

    public static final Locale RUSSIAN;

    static {
        RUSSIAN = new Locale("ru_RU");
    }

    @Autowired
    private ApplicationContext context;

    private final Callback<Class<?>, Object> controllerFactory;

    public FXMLLoaderService () {
        controllerFactory = (Class<?> param) -> context.getBean(param);
    }

    public <T extends Object> T loadNode (String fxmlFile) {
        return loadNode(fxmlFile, (String) null);
    }

    public <T extends Object> T loadNode (String fxmlFile, String localizationFile) {
        return localizationFile == null
               ? loadNode(fxmlFile, (ResourceBundle) null)
               : loadNode(fxmlFile, ResourceBundle.getBundle(localizationFile, RUSSIAN));
    }

    @SneakyThrows
    public <T extends Object> T loadNode (String fxmlFile, ResourceBundle localization) {
        return FXMLLoader.load(getClass().getResource(fxmlFile), localization, null, controllerFactory);
    }

    public FXMLLoader createLoader (String fxmlFile) {
        return createLoader(fxmlFile, (String) null);
    }

    public FXMLLoader createLoader (String fxmlFile, String localizationFile) {
        return localizationFile == null
               ? createLoader(fxmlFile, (ResourceBundle) null)
               : createLoader(fxmlFile, ResourceBundle.getBundle(localizationFile, RUSSIAN));
    }

    public FXMLLoader createLoader (String fxmlFile, ResourceBundle localization) {
        return new FXMLLoader(getClass().getResource(fxmlFile), localization, null, controllerFactory);
    }
}
