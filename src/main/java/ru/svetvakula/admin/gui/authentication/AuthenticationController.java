/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.authentication;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.svetvakula.admin.service.authentication.AuthenticationService;
import ru.svetvakula.admin.service.authentication.exception.InvalidCodeException;
import ru.svetvakula.admin.service.authentication.exception.InvalidEmailException;
import ru.svetvakula.admin.service.authentication.exception.UnknownEmailException;
import ru.svetvakula.admin.service.authentication.exception.UnknownOrExpiredCodeException;
import ru.svetvakula.admin.gui.FXMLLoaderService;
import ru.svetvakula.admin.gui.dashboard.DashboardController;

import static javafx.scene.input.ScrollEvent.SCROLL;
import static javafx.util.Duration.millis;
import static javafx.util.Duration.millis;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 01.04.2016
 */
@Component
public class AuthenticationController implements Initializable {

    public final static String FXML = "/fxml/authentication/main.fxml";

    public final static String LOCALIZATION = "localization.authentication.text";

    @Autowired
    private FXMLLoaderService loader;

    @Autowired
    private AuthenticationService authenticationService;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private TextField textFieldEmail;

    @FXML
    private Label labelEmailError;

    @FXML
    private Button buttonNext;

    @FXML
    private TextField textFieldCode;

    @FXML
    private Label labelCodeError;

    @FXML
    private Button buttonTryAgain;

    @FXML
    private Button buttonSignIn;

    private Localization localization;

    private KeyFrame forwardAnimation;

    private KeyFrame backwardAnimation;

    private boolean scrollIsPermitted;

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        localization = new Localization(resources);

        val scrollPaneDuration = millis(250);
        forwardAnimation = new KeyFrame(scrollPaneDuration, new KeyValue(scrollPane.hvalueProperty(), 1));
        backwardAnimation = new KeyFrame(scrollPaneDuration, new KeyValue(scrollPane.hvalueProperty(), 0));

        buttonNext.disableProperty().bind(textFieldEmail.textProperty().isEmpty());
        buttonSignIn.disableProperty().bind(textFieldCode.textProperty().isEmpty());

        scrollPane.addEventFilter(SCROLL, event -> {
                              if (!scrollIsPermitted && event.getDeltaX() != 0) {
                                  event.consume();
                              }
                          });

        labelEmailError.visibleProperty().bind(labelEmailError.textProperty().isEmpty().not());
        labelCodeError.visibleProperty().bind(labelCodeError.textProperty().isEmpty().not());

        enableFirstScreen();
    }

    @FXML
    public void scrollForward (ActionEvent event) {
        labelEmailError.setText("");
        try {
            authenticationService.authenticate(textFieldEmail.getText());
        } catch (Throwable exception) {
            handleScrollForwardExceptions(exception);
            return;
        }
        enableSecondScreen();
        scroll(forwardAnimation);
    }

    @FXML
    public void scrollBackward (ActionEvent event) {
        enableFirstScreen();
        scroll(backwardAnimation);
    }

    @FXML
    public void signIn (ActionEvent event) {
        try {
            authenticationService.verify(textFieldCode.getText());
        } catch (Throwable ex) {
            handleSignInExceptions(ex);
            return;
        }

        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();

        Parent root = loader.loadNode(DashboardController.FXML, DashboardController.LOCALIZATION);
        root.getStylesheets().add(getClass().getResource("/style/dashboard.css").toExternalForm());

        Scene scene = new Scene(root);

        stage.setResizable(true);
        stage.setScene(scene);
        stage.centerOnScreen();

        stage.show();
    }

    private void enableFirstScreen () {
        textFieldCode.clear();
        labelCodeError.setText("");

        textFieldCode.setDisable(true);
        buttonTryAgain.setDisable(true);
        buttonTryAgain.setDefaultButton(false);
        buttonSignIn.setDefaultButton(false);

        textFieldEmail.setDisable(false);
        buttonNext.setDefaultButton(true);
    }

    private void enableSecondScreen () {
        textFieldEmail.setDisable(true);
        buttonNext.setDefaultButton(false);

        textFieldCode.setDisable(false);
        buttonTryAgain.setDisable(false);
        buttonSignIn.setDefaultButton(true);
    }

    private void handleScrollForwardExceptions (Throwable exception) {
        if (exception instanceof UnknownEmailException) {
            labelEmailError.setText(localization.unknownEmail());
        } else if (exception instanceof InvalidEmailException) {
            labelEmailError.setText(localization.invalidEmail());
        } else {
            labelEmailError.setText(localization.unknownError());
            exception.printStackTrace();
        }
    }

    private void handleSignInExceptions (Throwable exception) {
        textFieldCode.clear();
        textFieldCode.setDisable(true);

        buttonSignIn.setDefaultButton(false);

        buttonTryAgain.setDefaultButton(true);
        buttonTryAgain.setFocusTraversable(true);

        if (exception instanceof InvalidCodeException) {
            labelCodeError.setText(localization.invalidCode());
        } else if (exception instanceof UnknownOrExpiredCodeException) {
            labelCodeError.setText(localization.unknownOrExpiredCode());
        } else {
            labelCodeError.setText(localization.unknownError());
            exception.printStackTrace();
        }
    }

    private void scroll (KeyFrame keyFrame) {
        scrollIsPermitted = true;
        val timeline = new Timeline(keyFrame);
        timeline.setOnFinished(it -> scrollIsPermitted = false);
        timeline.play();
    }

    private class Localization {

        private final ResourceBundle resourceBundle;

        Localization (ResourceBundle resourceBundle) {
            this.resourceBundle = resourceBundle;
        }

        String invalidEmail () {
            return resourceBundle.getString("authentication.firstPage.error.invalidEmail");
        }

        String invalidCode () {
            return resourceBundle.getString("authentication.secondPage.error.invalidCode");
        }

        String unknownEmail () {
            return resourceBundle.getString("authentication.firstPage.error.unknownEmail");
        }

        String unknownOrExpiredCode () {
            return resourceBundle.getString("authentication.secondPage.error.unknownOrExpiredCode");
        }

        String unknownError () {
            return resourceBundle.getString("authentication.error.unknownError");
        }
    }
}
