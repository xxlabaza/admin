/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard;

import java.util.ResourceBundle;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import org.springframework.beans.factory.annotation.Autowired;
import ru.svetvakula.admin.gui.FXMLLoaderService;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 01.04.2016
 */
public abstract class AbstractMenuTreeItem implements MenuTreeItem {

    @Autowired
    private FXMLLoaderService loader;

    private final String fxmlFile;

    private final ResourceBundle localization;

    private final String menuItemTitle;

    private final String paneTitle;

    private Node view;

    private TreeItem<MenuTreeItem> treeItem;

    public AbstractMenuTreeItem (String fxmlFile, String localizationFile, String menuItemTitleKey, String paneTitleKey) {
        this.fxmlFile = fxmlFile;
        localization = ResourceBundle.getBundle(localizationFile, FXMLLoaderService.RUSSIAN);
        menuItemTitle = localization.getString(menuItemTitleKey);
        paneTitle = localization.getString(paneTitleKey);
    }

    @Override
    public final String getPaneTitle () {
        return paneTitle;
    }

    @Override
    public final Node getView () {
        if (view == null) {
            view = loader.loadNode(fxmlFile, localization);
        }
        return view;
    }

    @Override
    public final TreeItem<MenuTreeItem> getTreeItem () {
        if (treeItem == null) {
            treeItem = createTreeItem();
        }
        return treeItem;
    }

    @Override
    public final String toString () {
        return menuItemTitle;
    }

    protected TreeItem<MenuTreeItem> createTreeItem () {
        return new TreeItem<>(this);
    }
}
