/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import org.controlsfx.control.textfield.TextFields;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.svetvakula.admin.service.authentication.AuthenticationService;

import static java.util.stream.Collectors.toList;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 01.04.2016
 */
@Component
public class DashboardController implements Initializable {

    public final static String FXML = "/fxml/dashboard/main.fxml";

    public final static String LOCALIZATION = "localization.dashboard.text";

    @Autowired
    private List<RootMenuTreeItem> roots;

    @Autowired
    private AuthenticationService authenticationService;

    @FXML
    private TreeView<MenuTreeItem> treeViewMenu;

    @FXML
    private AnchorPane content;

    @FXML
    private Label labelManagementPaneTitle;

    @FXML
    private Label labelUserName;

    @FXML
    private ToolBar leftSidebarToolbar;

    private TextField searchField;

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        searchField = createSearchField(resources);
        leftSidebarToolbar.getItems().add(searchField);

        labelUserName.setText(authenticationService.getCurrentUser().get().getEmail());

        List<TreeItem<MenuTreeItem>> rootMenuItems = roots.stream()
                .map(MenuTreeItem::getTreeItem)
                .collect(toList());

        TreeItem<MenuTreeItem> mainRoot = new TreeItem<>();
        mainRoot.getChildren().addAll(rootMenuItems);

        treeViewMenu.setRoot(mainRoot);

        ObservableList<Node> children = content.getChildren();
        treeViewMenu.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null || newValue.getValue() == null) {
                return;
            }
            MenuTreeItem menuItem = newValue.getValue();
            children.clear();
            children.add(menuItem.getView());
            labelManagementPaneTitle.setText(menuItem.getPaneTitle());
        });
        treeViewMenu.getSelectionModel().selectFirst();
        treeViewMenu.getRoot().getChildren().forEach(this::expandItem);
    }

    private TextField createSearchField (ResourceBundle resources) {
        TextField textField = TextFields.createClearableTextField();
        textField.setPromptText(resources.getString("dashboard.sidebar.searchBox.prompt"));
        textField.setMaxWidth(Double.MAX_VALUE);
        textField.setMinWidth(100);
        textField.setPrefWidth(100);
        HBox.setHgrow(textField, Priority.ALWAYS);
        textField.setFont(Font.font(12));
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null || newValue.isEmpty()) {
                treeViewMenu.getRoot().getChildren().forEach(this::restoreTree);
            } else {
                searchSuitableItems(treeViewMenu.getRoot(), newValue);
            }
        });
        return textField;
    }

    private boolean searchSuitableItems (TreeItem<?> treeItem, String text) {
        boolean result = treeItem.toString().toUpperCase().contains(text.toUpperCase());
        for (TreeItem<?> child : treeItem.getChildren()) {
            if (searchSuitableItems(child, text)) {
                result = true;
            }
        }
        treeItem.setExpanded(result);
        return result;
    }

    private void restoreTree (TreeItem<?> treeItem) {
        treeItem.setExpanded(false);
        treeItem.getChildren().forEach(this::restoreTree);
    }

    private void expandItem (TreeItem<?> treeItem) {
        treeItem.setExpanded(true);
        treeItem.getChildren().forEach(this::expandItem);
    }
}
