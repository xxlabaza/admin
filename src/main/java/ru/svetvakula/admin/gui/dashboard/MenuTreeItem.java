/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard;

import javafx.scene.Node;
import javafx.scene.control.TreeItem;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 01.04.2016
 */
public interface MenuTreeItem {

    public TreeItem<MenuTreeItem> getTreeItem ();

    public Node getView ();

    public String getPaneTitle ();
}
