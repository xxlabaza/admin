/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 01.04.2016
 */
public interface RootMenuTreeItem extends MenuTreeItem {

}
