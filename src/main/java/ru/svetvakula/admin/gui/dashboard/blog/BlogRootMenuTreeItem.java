/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.blog;

import javafx.scene.control.TreeItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.svetvakula.admin.gui.dashboard.AbstractMenuTreeItem;
import ru.svetvakula.admin.gui.dashboard.MenuTreeItem;
import ru.svetvakula.admin.gui.dashboard.RootMenuTreeItem;
import ru.svetvakula.admin.gui.dashboard.blog.create.BlogCreateMenuTreeItem;
import ru.svetvakula.admin.gui.dashboard.blog.manage.BlogManageMenuTreeItem;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Component
public class BlogRootMenuTreeItem extends AbstractMenuTreeItem implements RootMenuTreeItem {

    @Autowired
    private BlogCreateMenuTreeItem createMenuItem;

    @Autowired
    private BlogManageMenuTreeItem manageMenuItem;

    public BlogRootMenuTreeItem () {
        super(BlogController.FXML,
              BlogController.LOCALIZATION,
              "dashboard.blog.menuItem.title",
              "dashboard.blog.pane.title"
        );
    }

    @Override
    protected TreeItem<MenuTreeItem> createTreeItem () {
        TreeItem<MenuTreeItem> treeItem = new TreeItem<>(this);
        treeItem.getChildren().addAll(
                createMenuItem.getTreeItem(),
                manageMenuItem.getTreeItem()
        );
        return treeItem;
    }
}
