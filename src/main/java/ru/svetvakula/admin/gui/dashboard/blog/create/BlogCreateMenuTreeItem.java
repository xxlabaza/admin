/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.blog.create;

import org.springframework.stereotype.Component;
import ru.svetvakula.admin.gui.dashboard.AbstractMenuTreeItem;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Component
public class BlogCreateMenuTreeItem extends AbstractMenuTreeItem {

    public BlogCreateMenuTreeItem () {
        super(BlogCreateController.FXML,
              BlogCreateController.LOCALIZATION,
              "dashboard.blog.create.menuItem.title",
              "dashboard.blog.create.pane.title"
        );
    }
}
