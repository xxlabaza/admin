/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.blog.manage;

import org.springframework.stereotype.Component;
import ru.svetvakula.admin.gui.dashboard.AbstractMenuTreeItem;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Component
public class BlogManageMenuTreeItem extends AbstractMenuTreeItem {

    public BlogManageMenuTreeItem () {
        super(BlogManageController.FXML,
              BlogManageController.LOCALIZATION,
              "dashboard.blog.manage.menuItem.title",
              "dashboard.blog.manage.pane.title"
        );
    }
}
