/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.product;

import javafx.scene.control.TreeItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.svetvakula.admin.gui.dashboard.AbstractMenuTreeItem;
import ru.svetvakula.admin.gui.dashboard.MenuTreeItem;
import ru.svetvakula.admin.gui.dashboard.RootMenuTreeItem;
import ru.svetvakula.admin.gui.dashboard.product.create.ProductCreateMenuTreeItem;
import ru.svetvakula.admin.gui.dashboard.product.manage.ProductManageMenuTreeItem;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 01.04.2016
 */
@Component
public class ProductRootMenuTreeItem extends AbstractMenuTreeItem implements RootMenuTreeItem {

    @Autowired
    private ProductCreateMenuTreeItem createMenuItem;

    @Autowired
    private ProductManageMenuTreeItem manageMenuItem;

    public ProductRootMenuTreeItem () {
        super(ProductController.FXML,
              ProductController.LOCALIZATION,
              "dashboard.product.menuItem.title",
              "dashboard.product.pane.title"
        );
    }

    @Override
    protected TreeItem<MenuTreeItem> createTreeItem () {
        TreeItem<MenuTreeItem> treeItem = new TreeItem<>(this);
        treeItem.getChildren().addAll(
                createMenuItem.getTreeItem(),
                manageMenuItem.getTreeItem()
        );
        return treeItem;
    }
}
