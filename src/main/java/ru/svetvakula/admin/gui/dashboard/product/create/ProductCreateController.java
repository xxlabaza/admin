/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.product.create;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import org.springframework.stereotype.Component;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 01.04.2016
 */
@Component
public class ProductCreateController implements Initializable {

    public final static String FXML = "/fxml/dashboard/product/create/main.fxml";

    public final static String LOCALIZATION = "localization.dashboard.product.create.text";

    @Override
    public void initialize (URL location, ResourceBundle resources) {
    }
}
