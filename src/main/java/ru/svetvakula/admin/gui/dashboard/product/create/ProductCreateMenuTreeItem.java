/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.product.create;

import org.springframework.stereotype.Component;
import ru.svetvakula.admin.gui.dashboard.AbstractMenuTreeItem;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 01.04.2016
 */
@Component
public class ProductCreateMenuTreeItem extends AbstractMenuTreeItem {

    public ProductCreateMenuTreeItem () {
        super(ProductCreateController.FXML,
              ProductCreateController.LOCALIZATION,
              "dashboard.product.create.menuItem.title",
              "dashboard.product.create.pane.title"
        );
    }
}
