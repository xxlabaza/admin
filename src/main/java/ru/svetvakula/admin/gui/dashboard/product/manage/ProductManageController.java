/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.product.manage;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.svetvakula.admin.gui.FXMLLoaderService;
import ru.svetvakula.admin.gui.dashboard.product.manage.editor.ProductManageEditorController;
import ru.svetvakula.admin.gui.dashboard.product.manage.view.ProductManageViewController;

import static javafx.scene.input.ScrollEvent.SCROLL;
import static javafx.util.Duration.millis;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 04.04.2016
 */
@Component
public class ProductManageController implements Initializable {

    public final static String FXML = "/fxml/dashboard/product/manage/main.fxml";

    public final static String LOCALIZATION = "localization.dashboard.product.manage.text";

    @Autowired
    private FXMLLoaderService loaderService;

    @Autowired
    private ProductManageViewController viewController;

    @Autowired
    private ProductManageEditorController editController;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private HBox hBox;

    @FXML
    private VBox leftPane;

    @FXML
    private VBox rightPane;

    private KeyFrame forwardAnimation;

    private KeyFrame backwardAnimation;

    private boolean scrollIsPermitted;

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        leftPane.getChildren().add((Node) loaderService.loadNode(ProductManageViewController.FXML, resources));
        rightPane.getChildren().add((Node) loaderService.loadNode(ProductManageEditorController.FXML, resources));

        initializeScrollPane();

        viewController.setTableRowDoubleClick(product -> {
            editController.setProduct(product);
            scroll(forwardAnimation);
        });
        editController.setButtonBackAction(event -> {
            scroll(backwardAnimation);
        });
    }

    private void initializeScrollPane () {
        scrollPane.addEventFilter(SCROLL, event -> {
                              if (!scrollIsPermitted && event.getDeltaX() != 0) {
                                  event.consume();
                              }
                          });
        val scrollPaneDuration = millis(300);
        forwardAnimation = new KeyFrame(scrollPaneDuration, new KeyValue(scrollPane.hvalueProperty(), 1));
        backwardAnimation = new KeyFrame(scrollPaneDuration, new KeyValue(scrollPane.hvalueProperty(), 0));

        hBox.prefWidthProperty().bind(scrollPane.widthProperty().multiply(2));
        hBox.prefHeightProperty().bind(scrollPane.heightProperty());

        leftPane.prefWidthProperty().bind(scrollPane.widthProperty());
        rightPane.prefWidthProperty().bind(leftPane.widthProperty());
    }

    private void scroll (KeyFrame keyFrame) {
        scrollIsPermitted = true;
        val timeline = new Timeline(keyFrame);
        timeline.setOnFinished(it -> scrollIsPermitted = false);
        timeline.play();
    }
}
