/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.product.manage;

import org.springframework.stereotype.Component;
import ru.svetvakula.admin.gui.dashboard.AbstractMenuTreeItem;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 04.04.2016
 */
@Component
public class ProductManageMenuTreeItem extends AbstractMenuTreeItem {

    public ProductManageMenuTreeItem () {
        super(ProductManageController.FXML,
              ProductManageController.LOCALIZATION,
              "dashboard.product.manage.menuItem.title",
              "dashboard.product.manage.pane.title"
        );
    }
}
