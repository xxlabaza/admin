/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.product.manage.editor;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Stream;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import lombok.Data;
import org.controlsfx.control.ListSelectionView;
import org.controlsfx.control.PropertySheet;
import org.controlsfx.control.PropertySheet.Item;
import org.controlsfx.property.BeanProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.svetvakula.admin.gui.util.ChangeablePropertyEditorFactory;
import ru.svetvakula.admin.gui.util.ChangeableTextAreaEditor;
import ru.svetvakula.admin.service.portfolio.category.Category;
import ru.svetvakula.admin.service.portfolio.category.CategoryService;
import ru.svetvakula.admin.service.portfolio.product.Product;

import static java.util.stream.Collectors.toList;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 27.04.2016
 */
@Component
public class ProductManageEditorBeanController implements Initializable {

    public final static String FXML = "/fxml/dashboard/product/manage/editor/bean.fxml";

    private final static PropertyDescriptor[] PRODUCT_PROPERTY_DESCRIPTORS;

    static {
        PRODUCT_PROPERTY_DESCRIPTORS = new PropertyDescriptor[7];
        try {
            PRODUCT_PROPERTY_DESCRIPTORS[0] = new PropertyDescriptor("name", Product.class);
            PRODUCT_PROPERTY_DESCRIPTORS[1] = new PropertyDescriptor("size", Product.class);
            PRODUCT_PROPERTY_DESCRIPTORS[2] = new PropertyDescriptor("terms", Product.class);
            PRODUCT_PROPERTY_DESCRIPTORS[3] = new PropertyDescriptor("created", Product.class);
            PRODUCT_PROPERTY_DESCRIPTORS[4] = new PropertyDescriptor("views", Product.class);
            PRODUCT_PROPERTY_DESCRIPTORS[5] = new PropertyDescriptor("visible", Product.class);
            PRODUCT_PROPERTY_DESCRIPTORS[6] = new PropertyDescriptor("description", Product.class);
        } catch (IntrospectionException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Autowired
    private ChangeablePropertyEditorFactory editorFactory;

    @Autowired
    private CategoryService categoryService;

    @FXML
    private PropertySheet propertySheet;

    @FXML
    private ListSelectionView<Category> categoriesSelection;

    private ObservableSet<String> changedFields;

    public ObservableSet<String> changedFields () {
        return changedFields;
    }

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        propertySheet.setPropertyEditorFactory(editorFactory);
        changedFields = FXCollections.observableSet();

        categoriesSelection.setCellFactory((param) -> new ListCell<Category>() {

            @Override
            protected void updateItem (Category category, boolean empty) {
                super.updateItem(category, empty);
                if (category != null) {
                    setText(category.getName());
                }
            }
        });

        PRODUCT_PROPERTY_DESCRIPTORS[0].setDisplayName(
                resources.getString("dashboard.product.manage.tab.general.bean.product.name")
        );
        PRODUCT_PROPERTY_DESCRIPTORS[1].setDisplayName(
                resources.getString("dashboard.product.manage.tab.general.bean.product.size")
        );
        PRODUCT_PROPERTY_DESCRIPTORS[2].setDisplayName(
                resources.getString("dashboard.product.manage.tab.general.bean.product.terms")
        );
        PRODUCT_PROPERTY_DESCRIPTORS[3].setDisplayName(
                resources.getString("dashboard.product.manage.tab.general.bean.product.created")
        );
        PRODUCT_PROPERTY_DESCRIPTORS[4].setDisplayName(
                resources.getString("dashboard.product.manage.tab.general.bean.product.views")
        );
        PRODUCT_PROPERTY_DESCRIPTORS[5].setDisplayName(
                resources.getString("dashboard.product.manage.tab.general.bean.product.visible")
        );
        PRODUCT_PROPERTY_DESCRIPTORS[6].setDisplayName(
                resources.getString("dashboard.product.manage.tab.general.bean.product.description")
        );
        PRODUCT_PROPERTY_DESCRIPTORS[6].setPropertyEditorClass(ChangeableTextAreaEditor.class);
    }

    public void setProduct (Product product) {
        Product clone = new Product(product);

        List<Item> items = Stream.of(PRODUCT_PROPERTY_DESCRIPTORS)
                .map(descriptor -> new BeanProperty(clone, descriptor))
                .peek(it -> {
                    it.getObservableValue().get().addListener(new My(it));
                })
                .collect(toList());
        propertySheet.getItems().setAll(items);

        List<Category> allCategories = categoryService.findAll().getContent();
        List<Category> productCategories = clone.getCategories();
        if (productCategories != null && !productCategories.isEmpty()) {
            categoriesSelection.getTargetItems().setAll(productCategories);
            allCategories.removeIf(productCategories::contains);
        }
        categoriesSelection.getSourceItems().setAll(allCategories);
    }

    @Data
    class My implements ChangeListener<Object> {

        private final Object initialValue;

        private final String fieldName;

        My (BeanProperty beanProperty) {
            initialValue = beanProperty.getValue();
            fieldName = beanProperty.getPropertyDescriptor().getName();
        }

        @Override
        public void changed (ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
            boolean wasChanged = newValue == null
                                 ? newValue != initialValue
                                 : !newValue.equals(initialValue);

            if (wasChanged && !changedFields.contains(fieldName)) {
                changedFields.add(fieldName);
                return;
            }
            if (!wasChanged && changedFields.contains(fieldName)) {
                changedFields.remove(fieldName);
            }
        }
    }
}
