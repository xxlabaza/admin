/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.product.manage.editor;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.svetvakula.admin.gui.FXMLLoaderService;
import ru.svetvakula.admin.service.portfolio.product.Product;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 27.04.2016
 */
@Component
public class ProductManageEditorController implements Initializable {

    public final static String FXML = "/fxml/dashboard/product/manage/editor/main.fxml";

    @Autowired
    private FXMLLoaderService loaderService;

    @Autowired
    private ProductManageEditorBeanController beanController;

    @Autowired
    private ProductManageEditorPhotosController photosController;

    @FXML
    private TabPane tabPane;

    @FXML
    private Button buttonBack;

    @FXML
    private Button buttonSubmit;

    @FXML
    private Button buttonCancel;

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        tabPane.getTabs().addAll(
                (Tab) loaderService.loadNode(ProductManageEditorBeanController.FXML, resources),
                (Tab) loaderService.loadNode(ProductManageEditorPhotosController.FXML, resources)
        );

        buttonBack.disableProperty().bind(Bindings.size(beanController.changedFields()).isNotEqualTo(0));
        buttonSubmit.disableProperty().bind(Bindings.size(beanController.changedFields()).isEqualTo(0));
        buttonCancel.disableProperty().bind(Bindings.size(beanController.changedFields()).isEqualTo(0));
    }

    public void setProduct (Product product) {
        beanController.setProduct(product);
        photosController.showProductPhotos(product);
    }

    public void setButtonBackAction (EventHandler<ActionEvent> eventHandler) {
        buttonBack.setOnAction(eventHandler);
    }
}
