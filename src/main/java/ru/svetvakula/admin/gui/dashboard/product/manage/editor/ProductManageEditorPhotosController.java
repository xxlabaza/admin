/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.product.manage.editor;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.controlsfx.control.GridCell;
import org.controlsfx.control.GridView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.svetvakula.admin.AppProperties;
import ru.svetvakula.admin.service.portfolio.photo.Photo;
import ru.svetvakula.admin.service.portfolio.product.Product;

import static javafx.scene.input.KeyCode.LEFT;
import static javafx.scene.input.KeyCode.RIGHT;
import static javafx.scene.input.MouseButton.PRIMARY;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 27.04.2016
 */
@Component
public class ProductManageEditorPhotosController implements Initializable {

    public final static String FXML = "/fxml/dashboard/product/manage/editor/photos.fxml";

    private static final Lighting LIGHTING;

    static {
        LIGHTING = new Lighting();
        LIGHTING.setDiffuseConstant(0.5D);
        LIGHTING.setSpecularExponent(20.D);
        LIGHTING.setSurfaceScale(1.5D);
    }

    @Autowired
    private AppProperties appProperties;

    @FXML
    private GridView<Photo> gridViewPhotos;

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        gridViewPhotos.setCellFactory(image -> new Popa());
        gridViewPhotos.setCellHeight(220);
        gridViewPhotos.setCellWidth(220);
    }

    public void showProductPhotos (Product product) {
        gridViewPhotos.getItems().clear();
        product.getPhotos().stream().forEach(gridViewPhotos.getItems()::add);
    }

    private void showImagePreview (int imageIndex) {
        ImageView preview = new ImageView();
        preview.setImage(createImage(gridViewPhotos.getItems().get(imageIndex)));
        preview.setPreserveRatio(true);
        preview.setSmooth(true);

        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(preview);
        borderPane.setStyle("-fx-background-color: BLACK");

        Stage stage = new Stage();
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setWidth(primaryScreenBounds.getWidth());
        stage.setHeight(primaryScreenBounds.getHeight());
        stage.setResizable(false);
        stage.setFullScreen(true);

        Scene scene = new Scene(borderPane);
        scene.setOnMouseClicked(eventHandler -> stage.close());
        scene.setOnKeyPressed(new My(imageIndex) {

            @Override
            public void handle (Photo photo) {
                preview.setImage(createImage(photo));
            }
        });
        stage.setScene(scene);

        stage.centerOnScreen();
        stage.show();
    }

    private abstract class My implements EventHandler<KeyEvent> {

        private int currentIndex;

        My (int currentIndex) {
            this.currentIndex = currentIndex;
        }

        @Override
        public void handle (KeyEvent event) {
            KeyCode keyCode = event.getCode();
            Photo photo;
            if (keyCode == LEFT && currentIndex > 0) {
                currentIndex--;
            } else if (keyCode == RIGHT && currentIndex < gridViewPhotos.getItems().size() - 1) {
                currentIndex++;
            } else {
                return;
            }
            photo = gridViewPhotos.getItems().get(currentIndex);
            handle(photo);
        }

        abstract public void handle (Photo photo);
    }

    public class Popa extends GridCell<Photo> {

        private final ImageView imageView;

        public Popa () {
            getStyleClass().add("image-grid-cell");
            imageView = new ImageView();
            imageView.fitHeightProperty().bind(heightProperty());
            imageView.fitWidthProperty().bind(widthProperty());
            setOnMouseClicked(event -> {
                if (event.getButton() == PRIMARY) {
                    showImagePreview(getIndex());
                }
                event.consume();
            });
        }

        @Override
        protected void updateItem (Photo item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setGraphic(null);
            } else {
                Image image = createImage(item);
                imageView.setPreserveRatio(true);
                imageView.setSmooth(true);
                imageView.setImage(image);
                if (!item.getVisible()) {
                    imageView.setEffect(LIGHTING);
                }
                setGraphic(imageView);

                ContextMenu contextMenu = createContextMenu(item);
                setContextMenu(contextMenu);
            }
        }

        private ContextMenu createContextMenu (Photo photo) {
            MenuItem show = new MenuItem("Show");
            show.visibleProperty().bind(photo.visibleProperty().not());
            show.setOnAction(handler -> {
                photo.setVisible(Boolean.TRUE);
                imageView.setEffect(null);
            });

            MenuItem hide = new MenuItem("Hide");
            hide.visibleProperty().bind(photo.visibleProperty());
            hide.setOnAction(handler -> {
                photo.setVisible(Boolean.FALSE);
                imageView.setEffect(LIGHTING);
            });

            return new ContextMenu(show, hide);
        }
    }

    private Image createImage (Photo photo) {
        String url = new StringBuilder()
                .append(appProperties.getServer().getUrl())
                .append(photo.getUrl())
                .toString();
        return new Image(url);
    }
}
