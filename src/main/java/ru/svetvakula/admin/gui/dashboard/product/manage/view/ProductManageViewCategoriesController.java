/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.product.manage.view;

import java.net.URL;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.util.StringConverter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.svetvakula.admin.service.portfolio.category.Category;
import ru.svetvakula.admin.service.portfolio.category.CategoryService;

import static javafx.scene.control.Alert.AlertType.CONFIRMATION;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 27.04.2016
 */
@Slf4j
@Component
public class ProductManageViewCategoriesController implements Initializable {

    public final static String FXML = "/fxml/dashboard/product/manage/view/categories.fxml";

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductManageViewTableController tableController;

    @FXML
    private ListView<Category> categories;

    @FXML
    private MenuItem menuItemRemoveCategory;

    private final Alert alertDeleteCategory;

    private final MessageFormat deleteCategoryContentFormatter;

    public ProductManageViewCategoriesController () {
        deleteCategoryContentFormatter = new MessageFormat("");
        alertDeleteCategory = new Alert(CONFIRMATION);
    }

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        alertDeleteCategory.setTitle(resources.getString("dashboard.product.manage.list.alert.title"));
        alertDeleteCategory.setHeaderText(resources.getString("dashboard.product.manage.list.alert.header"));
        deleteCategoryContentFormatter.applyPattern(resources.getString("dashboard.product.manage.list.alert.content"));

        categories.setCellFactory(TextFieldListCell.forListView(new StringConverter<Category>() {

            @Override
            public String toString (Category category) {
                return category != null
                       ? category.getName()
                       : "";
            }

            @Override
            public Category fromString (String name) {
                Category category = categoryService.create(name);
                return category;
            }
        }));
        categories.setEditable(true);

        categories.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Category>() {

            @Override
            public void changed (ObservableValue<? extends Category> observable, Category oldValue, Category newValue) {
                String categoryAlias = newValue.getName().equals(categories.getItems().get(0).getName())
                                       ? null
                                       : newValue.getAlias();

                tableController.showProductsInCategory(categoryAlias);
            }
        });
        categories.setItems(FXCollections.observableArrayList(new Category("Все")));
        categories.getItems().addAll(categoryService.findAll().getContent());
        categories.getSelectionModel().select(0);

        menuItemRemoveCategory.disableProperty().bind(categories.getSelectionModel().selectedIndexProperty()
                .isEqualTo(0));
    }

    @FXML
    public void add (ActionEvent event) {
        val items = categories.getItems();
        items.add(new Category(""));
        categories.edit(items.size() - 1);
    }

    @FXML
    public void remove (ActionEvent event) {
        val selectedCategory = categories.getSelectionModel().getSelectedItem();

        val arguments = new Object[] {
            selectedCategory.getName()
        };
        alertDeleteCategory.setContentText(deleteCategoryContentFormatter.format(arguments));
        alertDeleteCategory.showAndWait().filter(ButtonType.OK::equals).ifPresent(it -> {
            categories.getItems().remove(selectedCategory);
            categoryService.delete(selectedCategory);
        });
    }
}
