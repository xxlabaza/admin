/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.product.manage.view;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.SplitPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.svetvakula.admin.gui.FXMLLoaderService;
import ru.svetvakula.admin.gui.dashboard.product.manage.Handler;
import ru.svetvakula.admin.service.portfolio.product.Product;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 27.04.2016
 */
@Component
public class ProductManageViewController implements Initializable {

    public final static String FXML = "/fxml/dashboard/product/manage/view/main.fxml";

    @Autowired
    private FXMLLoaderService loaderService;

    @Autowired
    private ProductManageViewTableController tableController;

    @FXML
    private SplitPane splitPane;

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        splitPane.getItems().addAll(
                (Node) loaderService.loadNode(ProductManageViewTableController.FXML, resources),
                (Node) loaderService.loadNode(ProductManageViewCategoriesController.FXML, resources)
        );
    }

    public void setTableRowDoubleClick (Handler<Product> handler) {
        tableController.setTableRowDoubleClick(handler);
    }
}
