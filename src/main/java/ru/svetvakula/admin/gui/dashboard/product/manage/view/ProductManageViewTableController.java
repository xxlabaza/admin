/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.product.manage.view;

import java.net.URL;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.MapChangeListener.Change;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.svetvakula.admin.feign.Filter;
import ru.svetvakula.admin.gui.dashboard.product.manage.Handler;
import ru.svetvakula.admin.service.portfolio.product.Product;
import ru.svetvakula.admin.service.portfolio.product.ProductService;
import ru.xxlabaza.javafx.table.FilteredTable;
import ru.xxlabaza.javafx.table.column.DateFilteredColumn;

import static java.util.stream.Collectors.toList;
import static ru.svetvakula.admin.feign.Filter.Operator.CONTAINS;
import static ru.svetvakula.admin.feign.Filter.Operator.EQUALS;
import static ru.svetvakula.admin.feign.Filter.Operator.GREATER_THAN_EQUALS;
import static ru.svetvakula.admin.feign.Filter.Operator.LESS_THAN_EQUALS;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 27.04.2016
 */
@Component
public class ProductManageViewTableController implements Initializable {

    public final static String FXML = "/fxml/dashboard/product/manage/view/table.fxml";

    @Autowired
    @Qualifier("dateTimeFormatter")
    private DateTimeFormatter dateTimeFormatter;

    @Autowired
    private ProductService productService;

    @FXML
    private FilteredTable<Product> tableViewProducts;

    private Filter[] currentFilters;

    private Filter currentCategoryFilter;

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        val columns = tableViewProducts.getColumns();
        ((TableColumn<Product, String>) columns.get(0)).setCellValueFactory(
                cell -> cell.getValue().nameProperty()
        );
        DateFilteredColumn<Product, ZonedDateTime> dateColumn = (DateFilteredColumn<Product, ZonedDateTime>) columns
                .get(1);
        dateColumn.setDateTimeFormatter(dateTimeFormatter);
        dateColumn.setCellValueFactory(
                cell -> cell.getValue().createdProperty()
        );
        ((TableColumn<Product, Integer>) columns.get(2)).setCellValueFactory(
                cell -> cell.getValue().viewsProperty().asObject()
        );
        ((TableColumn<Product, Boolean>) columns.get(3)).setCellValueFactory(
                cell -> cell.getValue().visibleProperty()
        );

        val visibleTrue = resources.getString("dashboard.product.manage.table.column.visible.comboBox.true");

        tableViewProducts.getFilters().addListener((Change<? extends String, ? extends Object> change) -> {
            List<Filter> filters = change.getMap().entrySet().stream()
                    .map(entry -> {
                        switch (entry.getKey().toString()) {
                        case "name":
                            return new Filter("name", CONTAINS, entry.getValue().toString());
                        case "created.from":
                            return null;
                        case "created.to":
                            return null;
                        case "views.from":
                            return new Filter("views", GREATER_THAN_EQUALS, entry.getValue().toString());
                        case "views.to":
                            return new Filter("views", LESS_THAN_EQUALS, entry.getValue().toString());
                        case "visible":
                            String value = entry.getValue().toString().equals(visibleTrue)
                                           ? "true"
                                           : "false";
                            return new Filter("visible", EQUALS, value);
                        default:
                            throw new RuntimeException();
                        }
                    })
                    .collect(toList());

            if (currentCategoryFilter != null) {
                filters.add(currentCategoryFilter);
            }
            showProducts(filters.toArray(new Filter[filters.size()]));
        });
        tableViewProducts.setOnResetFiltersAction(event -> {
            showProducts(currentCategoryFilter);
        });
        tableViewProducts.pageSizeValueProperty().addListener((observable, oldValue, newValue) -> {
            showProducts(1, newValue, currentFilters);
        });
        tableViewProducts.setOnRefreshTableAction(event -> {
            showProducts(tableViewProducts.getPageNumber(), tableViewProducts.getPageSize(), currentFilters);
        });
        tableViewProducts.pageNumberProperty().addListener((observable, oldValue, newValue) -> {
            showProducts(newValue.intValue(), tableViewProducts.getPageSize(), currentFilters);
        });
    }

    public void showProductsInCategory (String categoryAlias) {
        currentCategoryFilter = categoryAlias == null || categoryAlias.isEmpty()
                                ? null
                                : new Filter("categories.alias", CONTAINS, categoryAlias);

        tableViewProducts.resetFilters();
        showProducts(currentCategoryFilter);
    }

    public void setTableRowDoubleClick (Handler<Product> handler) {
        tableViewProducts.getSelectionModel().getTableView().setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                Product product = tableViewProducts.getSelectionModel().getSelectedItem();
                handler.handle(product);
            }
        });
    }

    private void showProducts (Filter... filters) {
        showProducts(1, tableViewProducts.getPageSize(), filters);
    }

    private void showProducts (int pageNumber, int pageSize, Filter... filters) {
        currentFilters = filters;
        val page = productService.findAll(pageNumber, pageSize, filters);

        tableViewProducts.setTotalItems(page.getTotal());
        tableViewProducts.setTotalPages(page.getPages());
        tableViewProducts.setPageNumber(page.getPage());
        tableViewProducts.getItems().setAll(page.getContent());
    }
}
