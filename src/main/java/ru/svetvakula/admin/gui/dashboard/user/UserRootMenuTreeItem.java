/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.user;

import javafx.scene.control.TreeItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.svetvakula.admin.gui.dashboard.AbstractMenuTreeItem;
import ru.svetvakula.admin.gui.dashboard.MenuTreeItem;
import ru.svetvakula.admin.gui.dashboard.RootMenuTreeItem;
import ru.svetvakula.admin.gui.dashboard.user.admin.UserAdminMenuTreeItem;
import ru.svetvakula.admin.gui.dashboard.user.create.UserCreateMenuTreeItem;
import ru.svetvakula.admin.gui.dashboard.user.manage.UserManageMenuTreeItem;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 04.04.2016
 */
@Component
public class UserRootMenuTreeItem extends AbstractMenuTreeItem implements RootMenuTreeItem {

    @Autowired
    private UserAdminMenuTreeItem adminMenuItem;

    @Autowired
    private UserCreateMenuTreeItem createMenuItem;

    @Autowired
    private UserManageMenuTreeItem manageMenuItem;

    public UserRootMenuTreeItem () {
        super(UserController.FXML,
              UserController.LOCALIZATION,
              "dashboard.user.menuItem.title",
              "dashboard.user.pane.title"
        );
    }

    @Override
    protected TreeItem<MenuTreeItem> createTreeItem () {
        TreeItem<MenuTreeItem> treeItem = new TreeItem<>(this);
        treeItem.getChildren().addAll(
                adminMenuItem.getTreeItem(),
                createMenuItem.getTreeItem(),
                manageMenuItem.getTreeItem()
        );
        return treeItem;
    }
}
