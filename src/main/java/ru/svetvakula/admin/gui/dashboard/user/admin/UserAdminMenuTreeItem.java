/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.user.admin;

import org.springframework.stereotype.Component;
import ru.svetvakula.admin.gui.dashboard.AbstractMenuTreeItem;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 04.04.2016
 */
@Component
public class UserAdminMenuTreeItem extends AbstractMenuTreeItem {

    public UserAdminMenuTreeItem () {
        super(UserAdminController.FXML,
              UserAdminController.LOCALIZATION,
              "dashboard.user.admin.menuItem.title",
              "dashboard.user.admin.pane.title"
        );
    }
}
