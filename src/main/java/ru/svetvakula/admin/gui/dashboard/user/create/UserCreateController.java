/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.user.create;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import org.springframework.stereotype.Component;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 04.04.2016
 */
@Component
public class UserCreateController implements Initializable {

    public final static String FXML = "/fxml/dashboard/user/create/main.fxml";

    public final static String LOCALIZATION = "localization.dashboard.user.create.text";

    @Override
    public void initialize (URL location, ResourceBundle resources) {
    }
}
