/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.dashboard.user.create;

import org.springframework.stereotype.Component;
import ru.svetvakula.admin.gui.dashboard.AbstractMenuTreeItem;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 04.04.2016
 */
@Component
public class UserCreateMenuTreeItem extends AbstractMenuTreeItem {

    public UserCreateMenuTreeItem () {
        super(UserCreateController.FXML,
              UserCreateController.LOCALIZATION,
              "dashboard.user.create.menuItem.title",
              "dashboard.user.create.pane.title"
        );
    }
}
