/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.util;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import org.controlsfx.control.PropertySheet;
import org.controlsfx.property.editor.AbstractPropertyEditor;

import static javafx.geometry.Pos.TOP_LEFT;
import static javafx.scene.layout.Priority.ALWAYS;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 28.04.2016
 */
public abstract class AbstractChangeableBeanEditor<T, C extends Node> extends AbstractPropertyEditor<T, HBox> {

    private C realControl;

    private Label labelChanged;

    private boolean initialValueWasSet;

    private T initialValue;

    public AbstractChangeableBeanEditor (PropertySheet.Item property, C control, boolean readonly) {
        super(property, new HBox(new Label(" "), control), readonly);

        getEditor().setAlignment(TOP_LEFT);
        getEditor().setSpacing(1);

        getLabelChanged().setPrefWidth(8);
        HBox.setHgrow(control, ALWAYS);

        getObservableValue().addListener((observable, oldValue, newValue) -> {
            String symbol = initialValue == newValue || (initialValue != null && initialValue.equals(newValue))
                            ? " "
                            : "*";
            getLabelChanged().setText(symbol);
        });
    }

    public AbstractChangeableBeanEditor (PropertySheet.Item property, C control) {
        this(property, control, !property.isEditable());
    }

    @Override
    public void setValue (T value) {
        if (!initialValueWasSet) {
            initialValue = value;
            initialValueWasSet = true;
        }
        setControlValue(value);
    }

    protected abstract void setControlValue (T value);

    protected C getRealControl () {
        if (realControl == null) {
            realControl = (C) getEditor().getChildren().get(1);
        }
        return realControl;
    }

    private Label getLabelChanged () {
        if (labelChanged == null) {
            labelChanged = (Label) getEditor().getChildren().get(0);
        }
        return labelChanged;
    }
}
