/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.util;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.util.Collection;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.controlsfx.control.PropertySheet.Item;
import org.controlsfx.property.editor.PropertyEditor;
import ru.xxlabaza.javafx.table.NumericField;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 29.04.2016
 */
public final class ChangeableEditors {

    public static PropertyEditor<?> createTextEditor (Item item) {
        return new AbstractChangeableBeanEditor<String, TextField>(item, new TextField()) {

            @Override
            protected void setControlValue (String value) {
                getRealControl().setText(value);
            }

            @Override
            protected ObservableValue<String> getObservableValue () {
                return getRealControl().textProperty();
            }
        };
    }

    public static PropertyEditor<?> createTextAreaEditor (Item item) {
        return new AbstractChangeableBeanEditor<String, TextArea>(item, new TextArea()) {

            {
                getRealControl().setWrapText(true);
            }

            @Override
            protected void setControlValue (String value) {
                getRealControl().setText(value);
            }

            @Override
            protected ObservableValue<String> getObservableValue () {
                return getRealControl().textProperty();
            }
        };
    }

    public static PropertyEditor<?> createNumericEditor (Item item) {
        return new AbstractChangeableBeanEditor<Number, NumericField>(item, new NumericField(
                                                                      (Class<? extends Number>) item
                                                                      .getType())) {

            private Class<? extends Number> sourceClass = (Class<? extends Number>) item.getType(); //Double.class;

            @Override
            public Number getValue () {
                try {
                    return sourceClass.getConstructor(String.class).newInstance(getRealControl().getText());
                } catch (InstantiationException | IllegalAccessException | IllegalArgumentException |
                         InvocationTargetException | NoSuchMethodException | SecurityException ex) {
                    ex.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void setControlValue (Number value) {
                sourceClass = (Class<? extends Number>) value.getClass();
                getRealControl().setText(value.toString());
            }

            @Override
            protected ObservableValue<Number> getObservableValue () {
                return getRealControl().valueProperty();
            }
        };
    }

    public static PropertyEditor<?> createCheckEditor (Item item) {
        return new AbstractChangeableBeanEditor<Boolean, CheckBox>(item, new CheckBox()) {

            @Override
            protected void setControlValue (Boolean value) {
                getRealControl().setSelected(value);
            }

            @Override
            protected ObservableValue<Boolean> getObservableValue () {
                return getRealControl().selectedProperty();
            }
        };
    }

    public static <T> PropertyEditor<?> createChoiceEditor (Item item, final Collection<T> choices) {
        return new AbstractChangeableBeanEditor<T, ComboBox<T>>(item, new ComboBox<>()) {

            {
                getRealControl().setItems(FXCollections.observableArrayList(choices));
            }

            @Override
            protected void setControlValue (T value) {
                getRealControl().getSelectionModel().select(value);
            }

            @Override
            protected ObservableValue<T> getObservableValue () {
                return getRealControl().getSelectionModel().selectedItemProperty();
            }
        };
    }

    public static PropertyEditor<?> createDateEditor (Item item) {
        return new AbstractChangeableBeanEditor<LocalDate, DatePicker>(item, new DatePicker()) {

            @Override
            protected void setControlValue (LocalDate value) {
                getRealControl().setValue(value);
            }

            @Override
            protected ObservableValue<LocalDate> getObservableValue () {
                return getRealControl().valueProperty();
            }
        };
    }

    public static PropertyEditor<?> createReadOnlyDateTimeEditor (Item item, DateTimeFormatter formatter) {
        return new AbstractChangeableBeanEditor<Temporal, TextField>(item, new TextField()) {

            {
                getEditor().setDisable(true);
            }

            @Override
            public void setValue (Temporal value) {
                getRealControl().setText(formatter.format(value));
            }

            @Override
            protected ObservableValue<Temporal> getObservableValue () {
                return new SimpleObjectProperty<>();
            }

            @Override
            protected void setControlValue (Temporal value) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
    }

    private ChangeableEditors () {
    }
}
