/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.util.Arrays;
import java.util.Optional;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.util.Callback;
import org.controlsfx.control.PropertySheet;
import org.controlsfx.property.editor.Editors;
import org.controlsfx.property.editor.PropertyEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 29.04.2016
 */
@Component
public class ChangeablePropertyEditorFactory implements Callback<PropertySheet.Item, PropertyEditor<?>> {

    private static Class<?>[] numericTypes = new Class[] {
        byte.class, Byte.class,
        short.class, Short.class,
        int.class, Integer.class,
        long.class, Long.class,
        float.class, Float.class,
        double.class, Double.class,
        BigInteger.class, BigDecimal.class
    };

    // there should be better ways to do this
    private static boolean isNumber (Class<?> type) {
        if (type == null) {
            return false;
        }
        for (Class<?> cls : numericTypes) {
            if (type == cls) {
                return true;
            }
        }
        return false;
    }

    @Autowired
    @Qualifier("dateTimeFormatter")
    private DateTimeFormatter dateTimeFormatter;

    @Override
    public PropertyEditor<?> call (PropertySheet.Item item) {
        Class<?> type = item.getType();

        //TODO: add support for char and collection editors
        if (item.getPropertyEditorClass().isPresent()) {
            Optional<PropertyEditor<?>> ed = Editors.createCustomEditor(item);
            if (ed.isPresent()) {
                return ed.get();
            }
        }

        if (/*type != null &&*/type == String.class) {
            return ChangeableEditors.createTextEditor(item);
        }

        if (/*type != null &&*/isNumber(type)) {
            return ChangeableEditors.createNumericEditor(item);
        }

        if (/*type != null &&*/(type == boolean.class || type == Boolean.class)) {
            return ChangeableEditors.createCheckEditor(item);
        }

        if (/*type != null &&*/type == LocalDate.class) {
            return ChangeableEditors.createDateEditor(item);
        }
        if (Temporal.class.isAssignableFrom(type)) {
            return ChangeableEditors.createReadOnlyDateTimeEditor(item, dateTimeFormatter);
        }

        if (/*type != null &&*/type == Color.class || type == Paint.class) {
            return Editors.createColorEditor(item);
        }

        if (type != null && type.isEnum()) {
            return ChangeableEditors.createChoiceEditor(item, Arrays.<Object>asList(type.getEnumConstants()));
        }

        if (/*type != null &&*/type == Font.class) {
            return Editors.createFontEditor(item);
        }

        return null;
    }
}
