/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.util;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextArea;
import org.controlsfx.control.PropertySheet;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 28.04.2016
 */
public class ChangeableTextAreaEditor extends AbstractChangeableBeanEditor<String, TextArea> {

    public ChangeableTextAreaEditor (PropertySheet.Item property) {
        super(property, new TextArea());
        getRealControl().setWrapText(true);
    }

    @Override
    protected ObservableValue<String> getObservableValue () {
        return getRealControl().textProperty();
    }

    @Override
    protected void setControlValue (String value) {
        getRealControl().setText(value);
    }
}
