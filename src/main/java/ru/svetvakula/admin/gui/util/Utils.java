/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.gui.util;

import java.text.DateFormat;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.util.Date;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import static javafx.geometry.Pos.CENTER;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 28.04.2016
 */
public final class Utils {

    public static <ROW, T extends Temporal> Callback<TableColumn<ROW, T>, TableCell<ROW, T>> getDateCell (
            DateTimeFormatter format) {
        return column -> {
            return new TableCell<ROW, T>() {

                {
                    setAlignment(CENTER);
                }

                @Override
                protected void updateItem (T item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setText(null);
                    } else {
                        setText(format.format(item));
                    }
                }
            };
        };
    }

    public static <ROW, T extends Date> Callback<TableColumn<ROW, T>, TableCell<ROW, T>> getDateCell (
            DateFormat format
    ) {
        return column -> {
            return new TableCell<ROW, T>() {

                {
                    setAlignment(CENTER);
                }

                @Override
                protected void updateItem (T item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setText(null);
                    } else {
                        setText(format.format(item));
                    }
                }
            };
        };
    }

    private Utils () {
    }
}
