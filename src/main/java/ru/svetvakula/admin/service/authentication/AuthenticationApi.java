/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.authentication;

import feign.Feign;
import feign.Headers;
import feign.Param;
import feign.RequestInterceptor;
import feign.RequestLine;
import feign.Response;
import feign.codec.Encoder;
import feign.codec.ErrorDecoder;
import feign.jackson.JacksonDecoder;
import java.util.List;
import ru.svetvakula.admin.service.authentication.exception.InvalidCodeException;
import ru.svetvakula.admin.service.authentication.exception.InvalidEmailException;
import ru.svetvakula.admin.service.authentication.exception.NotAuthenticatedException;
import ru.svetvakula.admin.service.authentication.exception.UnknownEmailException;
import ru.svetvakula.admin.service.authentication.exception.UnknownOrExpiredCodeException;
import ru.svetvakula.admin.service.user.User;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 02.04.2016
 */
interface AuthenticationApi {

    @RequestLine("GET /api/me")
    @Headers({
        "X-POPA-BOOM:   {uid}",
        "Accept:        application/json"
    })
    User getMe (@Param("uid") String uid) throws NotAuthenticatedException;

    @RequestLine("POST /api/admin/authorization")
    int authenticate (@Param("email") String email) throws UnknownEmailException, InvalidEmailException;

    @RequestLine("GET /api/admin/verify/{code}")
    @Headers("Accept: application/json")
    Response verify (@Param("code") String code) throws InvalidCodeException, UnknownOrExpiredCodeException;

    @RequestLine("DELETE /api/detach")
    void detach ();

    static AuthenticationApi create (String serverUrl,
                                     Encoder encoder,
                                     JacksonDecoder jacksonDecoder,
                                     List<RequestInterceptor> interceptors,
                                     ErrorDecoder errorDecoder
    ) {
        return Feign.builder()
                .encoder(encoder)
                .decoder(jacksonDecoder)
                .requestInterceptors(interceptors)
                .errorDecoder(errorDecoder)
                .target(AuthenticationApi.class, serverUrl);
    }
}
