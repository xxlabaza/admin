/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.authentication;

import feign.Response;
import feign.jackson.JacksonDecoder;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.svetvakula.admin.AppProperties;
import ru.svetvakula.admin.feign.FeignErrorDecoder;
import ru.svetvakula.admin.feign.exception.InvalidResponseException;
import ru.svetvakula.admin.feign.exception.UnauthorizedException;
import ru.svetvakula.admin.service.authentication.exception.InvalidCodeException;
import ru.svetvakula.admin.service.authentication.exception.InvalidEmailException;
import ru.svetvakula.admin.service.authentication.exception.UnknownEmailException;
import ru.svetvakula.admin.service.authentication.exception.UnknownOrExpiredCodeException;
import ru.svetvakula.admin.service.storage.StorageService;
import ru.svetvakula.admin.service.user.User;

import static java.lang.Boolean.FALSE;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 02.04.2016
 */
@Service
public class AuthenticationService {

    @Autowired
    private JacksonDecoder jacksonDecoder;

    @Autowired
    private FeignErrorDecoder feignErrorDecoder;

    @Autowired
    private StorageService storageService;

    @Autowired
    private AuthenticationApi authenticationApi;

    private final String headerName;

    private User currentUser;

    private String uid;

    @Autowired
    public AuthenticationService (AppProperties appProperties) {
        headerName = appProperties.getServer().getSecurity().getHeaderName();
    }

    public boolean isAuthenticated () {
        if (uid != null && isValidKey(uid)) {
            return true;
        } else {
            uid = null;
        }
        return storageService.loadKey()
                .map(this::isValidKey)
                .orElse(FALSE);
    }

    public int authenticate (String email) throws UnknownEmailException, InvalidEmailException {
        return authenticationApi.authenticate(email);
    }

    @SneakyThrows
    public User verify (String code) throws InvalidCodeException,
                                            UnknownOrExpiredCodeException,
                                            InvalidResponseException {
        Response response = authenticationApi.verify(code);
        if (response.status() < 200 || response.status() > 299) {
            throw feignErrorDecoder.decode("AuthenticationApi#verify(String)", response);
        }

        Map<String, Collection<String>> headers = response.headers();
        if (headers == null || !headers.containsKey(headerName)) {
            throw new InvalidResponseException();
        }

        uid = headers.get(headerName).stream()
                .findFirst()
                .orElseThrow(InvalidResponseException::new);

        storageService.saveKey(uid);

        currentUser = (User) jacksonDecoder.decode(response, User.class);
        return currentUser;
    }

    public Optional<User> getCurrentUser () {
        return Optional.ofNullable(currentUser);
    }

    public Optional<String> getUid () {
        return Optional.ofNullable(uid);
    }

    private boolean isValidKey (String uid) {
        try {
            currentUser = authenticationApi.getMe(uid);
        } catch (UnauthorizedException ex) {
            return false;
        }
        this.uid = uid;
        return currentUser != null;
    }
}
