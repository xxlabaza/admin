/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.authentication.exception;

import ru.svetvakula.admin.feign.ErrorCode;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 04.04.2016
 */
@ErrorCode(404)
public class UnknownOrExpiredCodeException extends RuntimeException {

    private static final long serialVersionUID = 3718312238029207525L;

}
