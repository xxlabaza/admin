/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.blog.article;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Data;
import ru.svetvakula.admin.service.blog.tag.Tag;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Data
public class Article implements Serializable {

    private static final long serialVersionUID = 3590716280478953164L;

    private String alias;

    private String title;

    private String content;

    private Date created;

    private Boolean visible;

    private List<Tag> tags;
}
