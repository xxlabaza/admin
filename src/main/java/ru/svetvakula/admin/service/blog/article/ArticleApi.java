/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.blog.article;

import feign.Feign;
import feign.Headers;
import feign.Param;
import feign.RequestInterceptor;
import feign.RequestLine;
import feign.codec.Encoder;
import feign.codec.ErrorDecoder;
import feign.jackson.JacksonDecoder;
import java.util.List;
import ru.svetvakula.admin.feign.Page;
import ru.svetvakula.admin.service.blog.tag.Tag;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Headers({
    "Accept:        application/json",
    "Content-Type:  application/json"
})
interface ArticleApi {

    @RequestLine("POST /api/article")
    Article create (Article article);

    @RequestLine("GET /api/article")
    Page<Article> findAll ();

    @RequestLine("GET /api/article/{alias}")
    Article find (@Param("alias") String alias);

    @RequestLine("GET /api/article/{alias}/tag")
    List<Tag> getTags (@Param("alias") String alias);

    @RequestLine("PATCH /api/article/{alias}")
    Article patch (@Param("alias") String alias, Article updates);

    @RequestLine("PUT /api/article/{alias}/tag/{tags_aliases}")
    List<Tag> setTags (@Param("alias") String alias, @Param("tags_aliases") String tags);

    @RequestLine("PATCH /api/article/{alias}/tag/{tags_aliases}")
    List<Tag> addTags (@Param("alias") String alias, @Param("tags_aliases") String tags);

    @RequestLine("DELETE /api/article/{aliases}")
    void delete (@Param("aliases") String aliases);

    @RequestLine("DELETE /api/article/{alias}/tag/{tags_aliases}")
    List<Tag> deleteTags (@Param("alias") String alias, @Param("tags_aliases") String tags);

    static ArticleApi create (String serverUrl,
                              Encoder encoder,
                              JacksonDecoder jacksonDecoder,
                              List<RequestInterceptor> interceptors,
                              ErrorDecoder errorDecoder
    ) {
        return Feign.builder()
                .encoder(encoder)
                .decoder(jacksonDecoder)
                .requestInterceptors(interceptors)
                .errorDecoder(errorDecoder)
                .target(ArticleApi.class, serverUrl);
    }
}
