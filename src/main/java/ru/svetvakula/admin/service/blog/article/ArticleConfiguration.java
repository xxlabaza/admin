/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.blog.article;

import feign.RequestInterceptor;
import feign.codec.Encoder;
import feign.jackson.JacksonDecoder;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.svetvakula.admin.AppProperties;
import ru.svetvakula.admin.feign.FeignErrorDecoder;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Configuration
class ArticleConfiguration {

    @Bean
    ArticleApi articleApi (AppProperties appProperties,
                           Encoder encoder,
                           JacksonDecoder jacksonDecoder,
                           List<RequestInterceptor> interceptors,
                           FeignErrorDecoder errorDecoder
    ) {
        return ArticleApi.create(
                appProperties.getServer().getUrl(),
                encoder,
                jacksonDecoder,
                interceptors,
                errorDecoder
        );
    }
}
