/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.blog.article;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.svetvakula.admin.feign.Page;
import ru.svetvakula.admin.service.blog.tag.Tag;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Service
public class ArticleService {

    @Autowired
    private ArticleApi articleApi;

    public Article create (Article article) {
        return articleApi.create(article);
    }

    public Page<Article> findAll () {
        return articleApi.findAll();
    }

    public Article find (String alias) {
        return articleApi.find(alias);
    }

    public List<Tag> getTags (String alias) {
        return articleApi.getTags(alias);
    }

    public Article patch (String alias, Article updates) {
        return articleApi.patch(alias, updates);
    }

//    public List<Tag> setTags (String alias, List<String> tags) {
//        return articleApi.setTags(alias, tags);
//    }
//    public List<Tag> addTags (String alias, List<String> tags) {
//        return articleApi.addTags(alias, tags);
//    }
//    public void delete (List<String> aliases) {
//        articleApi.delete(aliases);
//    }
//    public List<Tag> deleteTags (String alias, List<String> tags) {
//        return articleApi.deleteTags(alias, tags);
//    }
}
