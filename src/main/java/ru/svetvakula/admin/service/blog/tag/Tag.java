/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.blog.tag;

import java.io.Serializable;
import lombok.Data;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Data
public class Tag implements Serializable {

    private static final long serialVersionUID = -3519501583179765496L;

    private String name;

    private String alias;
}
