/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.blog.tag;

import feign.Feign;
import feign.Headers;
import feign.RequestInterceptor;
import feign.RequestLine;
import feign.codec.Encoder;
import feign.codec.ErrorDecoder;
import feign.jackson.JacksonDecoder;
import java.util.List;
import ru.svetvakula.admin.feign.Page;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Headers("Content-Type: application/json")
interface TagApi {

    @RequestLine("GET /api/tag")
    Page<Tag> findAll ();

    static TagApi create (String serverUrl,
                          Encoder encoder,
                          JacksonDecoder jacksonDecoder,
                          List<RequestInterceptor> interceptors,
                          ErrorDecoder errorDecoder
    ) {
        return Feign.builder()
                .encoder(encoder)
                .decoder(jacksonDecoder)
                .requestInterceptors(interceptors)
                .errorDecoder(errorDecoder)
                .target(TagApi.class, serverUrl);
    }
}
