/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.file;

import feign.Feign;
import feign.Headers;
import feign.Param;
import feign.RequestInterceptor;
import feign.RequestLine;
import feign.codec.Encoder;
import feign.codec.ErrorDecoder;
import feign.jackson.JacksonDecoder;
import java.util.List;
import ru.svetvakula.admin.feign.Page;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Headers({
    "Accept:        application/json",
    "Content-Type:  application/json"
})
interface FileApi {

    @RequestLine("POST /api/file")
    FileInfo upload ();

    @RequestLine("GET /api/file")
    Page<FileInfo> findAll ();

    @RequestLine("GET /api/file/{hash}/info")
    FileInfo find (@Param("hash") String hash);

    @RequestLine("GET /api/file/{hash}")
    Object download (@Param("hash") String hash);

    @RequestLine("DELETE /api/file/{hash}")
    void delete (@Param("hash") String hash);

    static FileApi create (String serverUrl,
                           Encoder encoder,
                           JacksonDecoder jacksonDecoder,
                           List<RequestInterceptor> interceptors,
                           ErrorDecoder errorDecoder
    ) {
        return Feign.builder()
                .encoder(encoder)
                .decoder(jacksonDecoder)
                .requestInterceptors(interceptors)
                .errorDecoder(errorDecoder)
                .target(FileApi.class, serverUrl);
    }
}
