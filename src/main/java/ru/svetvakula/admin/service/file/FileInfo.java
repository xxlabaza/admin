/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.file;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Data
public class FileInfo implements Serializable {

    private static final long serialVersionUID = 1986708786954860406L;

    private String hash;

    private String checksum;

    private String name;

    private Date created;

    private Long size;

    @JsonProperty("public")
    private boolean isPublic;
}
