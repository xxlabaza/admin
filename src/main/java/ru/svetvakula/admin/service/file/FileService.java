/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.svetvakula.admin.feign.Page;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Service
public class FileService {

    @Autowired
    private FileApi fileApi;

    public FileInfo upload () {
        return fileApi.upload();
    }

    public Page<FileInfo> findAll () {
        return fileApi.findAll();
    }

    public FileInfo find (String hash) {
        return fileApi.find(hash);
    }

    public Object download (String hash) {
        return fileApi.download(hash);
    }

    public void delete (String hash) {
        fileApi.delete(hash);
    }
}
