/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.portfolio.category;

import java.io.Serializable;
import lombok.Data;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 25.04.2016
 */
@Data
public class Category implements Serializable {

    private static final long serialVersionUID = -5886254743896593072L;

    private String name;

    private String alias;

    public Category () {
    }

    public Category (String name) {
        this.name = name;
    }
}
