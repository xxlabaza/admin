/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.portfolio.category;

import feign.Feign;
import feign.Headers;
import feign.Param;
import feign.RequestInterceptor;
import feign.RequestLine;
import feign.codec.Encoder;
import feign.codec.ErrorDecoder;
import feign.jackson.JacksonDecoder;
import java.util.List;
import ru.svetvakula.admin.feign.Page;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 25.04.2016
 */
@Headers({
    "Accept:        application/json",
    "Content-Type:  application/json"
})
interface CategoryApi {

    @RequestLine("POST /api/category")
    Category create (Category category);

    @RequestLine("GET /api/category")
    Page<Category> findAll ();

    @RequestLine("GET /api/category/{alias}")
    Category find (@Param("alias") String alias);

    @RequestLine("PATCH /api/category/{alias}")
    Category patch (@Param("alias") String alias, Category updates);

    @RequestLine("DELETE /api/category/{aliases}")
    void delete (@Param("aliases") String aliases);

    static CategoryApi create (String serverUrl,
                               Encoder encoder,
                               JacksonDecoder jacksonDecoder,
                               List<RequestInterceptor> interceptors,
                               ErrorDecoder errorDecoder
    ) {
        return Feign.builder()
                .encoder(encoder)
                .decoder(jacksonDecoder)
                .requestInterceptors(interceptors)
                .errorDecoder(errorDecoder)
                .target(CategoryApi.class, serverUrl);
    }
}
