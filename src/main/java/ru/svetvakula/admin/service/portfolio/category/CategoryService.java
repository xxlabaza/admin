/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.portfolio.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.svetvakula.admin.feign.Page;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 25.04.2016
 */
@Service
public class CategoryService {

    @Autowired
    private CategoryApi categoryApi;

    public Category create (String name) {
        return categoryApi.create(new Category(name));
    }

    public Page<Category> findAll () {
        return categoryApi.findAll();
    }

    public Category find (String alias) {
        return categoryApi.find(alias);
    }

    public Category patch (String alias, Category updates) {
        return categoryApi.patch(alias, updates);
    }

    public void delete (Category category) {
        categoryApi.delete(category.getAlias());
    }
}
