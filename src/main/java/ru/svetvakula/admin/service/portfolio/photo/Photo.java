/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.portfolio.photo;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 05.05.2016
 */
@Data
@FieldDefaults(level = PRIVATE)
public class Photo {

    String uid;

    String url;

    Boolean main;

    BooleanProperty visibleProperty;

    public Photo () {
        visibleProperty = new SimpleBooleanProperty(false);
    }

    public void setVisible (Boolean visible) {
        this.visibleProperty.setValue(visible);
    }

    public Boolean getVisible () {
        return visibleProperty.getValue();
    }

    public BooleanProperty visibleProperty () {
        return visibleProperty;
    }
}
