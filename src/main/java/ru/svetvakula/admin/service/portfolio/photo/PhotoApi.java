/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.portfolio.photo;

import feign.Feign;
import feign.Headers;
import feign.Param;
import feign.RequestInterceptor;
import feign.RequestLine;
import feign.codec.Encoder;
import feign.codec.ErrorDecoder;
import feign.jackson.JacksonDecoder;
import java.io.File;
import java.util.List;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 05.05.2016
 */
@Headers({
    "Accept:        application/json",
    "Content-Type:  application/json"
})
interface PhotoApi {

    @RequestLine("POST /photo")
    @Headers("Content-Type: multipart/form-data")
    Photo upload (@Param("main") Boolean isMain, @Param("photo") File photo);

    static PhotoApi create (String serverUrl,
                            Encoder encoder,
                            JacksonDecoder jacksonDecoder,
                            List<RequestInterceptor> interceptors,
                            ErrorDecoder errorDecoder
    ) {
        return Feign.builder()
                .encoder(encoder)
                .decoder(jacksonDecoder)
                .requestInterceptors(interceptors)
                .errorDecoder(errorDecoder)
                .target(PhotoApi.class, serverUrl);
    }
}
