/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.portfolio.photo;

import java.io.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 05.05.2016
 */
@Service
public class PhotoService {

    @Autowired
    private PhotoApi photoApi;

    public Photo upload (Boolean isMain, File file) {
        return photoApi.upload(isMain, file);
    }
}
