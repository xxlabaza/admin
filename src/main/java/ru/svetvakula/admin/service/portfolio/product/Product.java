/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.portfolio.product;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.Data;
import ru.svetvakula.admin.service.portfolio.category.Category;
import ru.svetvakula.admin.service.portfolio.photo.Photo;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 25.04.2016
 */
@Data
public class Product implements Serializable {

    private static final long serialVersionUID = 431992200562476289L;

    private Integer id;

    private final StringProperty nameProperty;

    private final StringProperty descriptionProperty;

    private final StringProperty sizeProperty;

    private final StringProperty termsProperty;

    private final ObjectProperty<ZonedDateTime> createdProperty;

    private final IntegerProperty viewsProperty;

    private final BooleanProperty visibleProperty;

    private final ObservableList<Category> categories;

    private final ObservableList<Photo> photos;

    public Product () {
        nameProperty = new SimpleStringProperty();
        descriptionProperty = new SimpleStringProperty();
        sizeProperty = new SimpleStringProperty();
        termsProperty = new SimpleStringProperty();
        createdProperty = new SimpleObjectProperty<>();
        viewsProperty = new SimpleIntegerProperty();
        visibleProperty = new SimpleBooleanProperty();
        categories = FXCollections.observableArrayList();
        photos = FXCollections.observableArrayList();
    }

    public Product (Product product) {
        this();
        copy(product);
    }

    public final void copy (Product product) {
        id = product.getId();
        nameProperty.set(product.getName());
        descriptionProperty.set(product.getDescription());
        sizeProperty.set(product.getSize());
        termsProperty.set(product.getTerms());
        createdProperty.setValue(product.getCreated());
        viewsProperty.setValue(product.getViews());
        visibleProperty.setValue(product.getVisible());
        categories.setAll(new ArrayList<>(product.getCategories()));
    }

    public String getName () {
        return nameProperty.getValue();
    }

    public void setName (String name) {
        this.nameProperty.setValue(name);
    }

    public String getDescription () {
        return descriptionProperty.getValue();
    }

    public void setDescription (String description) {
        this.descriptionProperty.setValue(description);
    }

    public String getSize () {
        return sizeProperty.getValue();
    }

    public void setSize (String size) {
        this.sizeProperty.setValue(size);
    }

    public String getTerms () {
        return termsProperty.getValue();
    }

    public void setTerms (String terms) {
        this.termsProperty.setValue(terms);
    }

    public ZonedDateTime getCreated () {
        return createdProperty.getValue();
    }

    public void setCreated (ZonedDateTime created) {
        this.createdProperty.setValue(created);
    }

    public Integer getViews () {
        return viewsProperty.getValue();
    }

    public void setViews (Integer views) {
        this.viewsProperty.setValue(views);
    }

    public Boolean getVisible () {
        return visibleProperty.getValue();
    }

    public void setVisible (Boolean visible) {
        this.visibleProperty.setValue(visible);
    }

    public StringProperty nameProperty () {
        return nameProperty;
    }

    public StringProperty descriptionProperty () {
        return descriptionProperty;
    }

    public StringProperty sizeProperty () {
        return sizeProperty;
    }

    public StringProperty termsProperty () {
        return termsProperty;
    }

    public ObjectProperty<ZonedDateTime> createdProperty () {
        return createdProperty;
    }

    public IntegerProperty viewsProperty () {
        return viewsProperty;
    }

    public BooleanProperty visibleProperty () {
        return visibleProperty;
    }

    public void setCategories (List<Category> categories) {
        this.categories.addAll(categories);
    }

    public void setPhotos (List<Photo> photos) {
        this.photos.addAll(photos);
    }
}
