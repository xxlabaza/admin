/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.portfolio.product;

import feign.Feign;
import feign.Headers;
import feign.Param;
import feign.QueryMap;
import feign.RequestInterceptor;
import feign.RequestLine;
import feign.codec.Encoder;
import feign.codec.ErrorDecoder;
import feign.jackson.JacksonDecoder;
import java.util.List;
import java.util.Map;
import ru.svetvakula.admin.feign.Page;
import ru.svetvakula.admin.service.portfolio.category.Category;
import ru.svetvakula.admin.service.portfolio.product.exception.ConflictProductException;
import ru.svetvakula.admin.service.portfolio.product.exception.ProductNotFoundException;
import ru.svetvakula.admin.service.portfolio.product.exception.ProductVersionConflictException;
import ru.svetvakula.admin.service.portfolio.product.exception.UnprocessableProductException;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 05.04.2016
 */
@Headers({
    "Accept:        application/json",
    "Content-Type:  application/json"
})
interface ProductApi {

    @RequestLine("POST /api/product")
    Product create (Product product)
            throws UnprocessableProductException,
                   ConflictProductException;

    @RequestLine("GET /api/product?page={page}&size={size}")
    Page<Product> findAll (@Param("page") int page, @Param("size") int size,
                           @QueryMap Map<String, Object> filtersAndSorts);

    @RequestLine("GET /api/product/{id}")
    Product find (@Param("id") Integer id)
            throws ProductNotFoundException;

    @RequestLine("GET /api/product/{id}/category")
    List<Category> getCategories (@Param("id") Integer id);

    @RequestLine("PATCH /api/product/{id}")
    Product patch (@Param("id") Integer id, Product updates)
            throws UnprocessableProductException,
                   ProductNotFoundException,
                   ConflictProductException,
                   ProductVersionConflictException;

    @RequestLine("PUT /api/product/{id}/category/{category_aliases}")
    List<Category> setCategories (@Param("id") Integer id, @Param("category_aliases") String categories);

    @RequestLine("PATCH /api/product/{id}/category/{category_aliases}")
    List<Category> addCategories (@Param("id") Integer id, @Param("category_aliases") String categories);

    @RequestLine("DELETE /api/product/{id}")
    Product delete (@Param("id") Integer id)
            throws ProductNotFoundException;

    @RequestLine("DELETE /api/product/{id}/category/{category_aliases}")
    List<Category> deleteCategories (@Param("id") Integer id, @Param("category_aliases") String categories);

    static ProductApi create (String serverUrl,
                              Encoder encoder,
                              JacksonDecoder jacksonDecoder,
                              List<RequestInterceptor> interceptors,
                              ErrorDecoder errorDecoder
    ) {
        return Feign.builder()
                .encoder(encoder)
                .decoder(jacksonDecoder)
                .requestInterceptors(interceptors)
                .errorDecoder(errorDecoder)
                .target(ProductApi.class, serverUrl);
    }
}
