/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.portfolio.product;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.svetvakula.admin.feign.Filter;
import ru.svetvakula.admin.feign.Page;
import ru.svetvakula.admin.feign.Sort;
import ru.svetvakula.admin.service.portfolio.category.Category;
import ru.svetvakula.admin.service.portfolio.product.exception.ConflictProductException;
import ru.svetvakula.admin.service.portfolio.product.exception.ProductNotFoundException;
import ru.svetvakula.admin.service.portfolio.product.exception.ProductVersionConflictException;
import ru.svetvakula.admin.service.portfolio.product.exception.UnprocessableProductException;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 05.04.2016
 */
@Service
public class ProductService {

    @Autowired
    private ProductApi productApi;

    public Product create (Product product) throws UnprocessableProductException, ConflictProductException {
        return productApi.create(product);
    }

    public Page<Product> findAll (int page, int size) {
        return findAll(page, size, Collections.emptyList(), Collections.emptyList());
    }

    public Page<Product> findAll (int page, int size, Filter... filters) {
        return findAll(page, size, Arrays.asList(filters), Collections.emptyList());
    }

    public Page<Product> findAll (int page, int size, Sort... sorts) {
        return findAll(page, size, Collections.emptyList(), Arrays.asList(sorts));
    }

    public Page<Product> findAll (int page, Integer size, List<Filter> filters, List<Sort> sorts) {
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("filter", filters);
        queryMap.put("sort", sorts);
        return productApi.findAll(page, size, queryMap);
    }

    public Product find (Integer id) throws ProductNotFoundException {
        return productApi.find(id);
    }

    public List<Category> getCategories (Integer id) {
        return productApi.getCategories(id);
    }

    public Product patch (Integer id, Product updates) throws UnprocessableProductException, ProductNotFoundException,
                                                              ConflictProductException, ProductVersionConflictException {
        return productApi.patch(id, updates);
    }

//    public List<Category> setCategories (Integer id, List<String> categories) {
//        return productApi.setCategories(id, categories);
//    }
//
//    public List<Category> addCategories (Integer id, List<String> categories) {
//        return productApi.addCategories(id, categories);
//    }
    public Product delete (Integer id) throws ProductNotFoundException {
        return productApi.delete(id);
    }

//    public List<Category> deleteCategories (Integer id, List<String> categories) {
//        return productApi.deleteCategories(id, categories);
//    }
}
