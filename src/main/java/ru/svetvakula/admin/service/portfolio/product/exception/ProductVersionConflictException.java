/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.portfolio.product.exception;

import ru.svetvakula.admin.feign.ErrorCode;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 05.04.2016
 */
@ErrorCode(
        value = 409,
        reason = "Version conflict"
)
public class ProductVersionConflictException extends RuntimeException {

}
