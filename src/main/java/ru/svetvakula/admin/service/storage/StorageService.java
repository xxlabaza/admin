/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.storage;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.Optional;
import lombok.SneakyThrows;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.svetvakula.admin.AppProperties;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 01.04.2016
 */
@Service
public class StorageService {

    private final Path home;

    private final String keyFileExtension;

    private final PathMatcher keyMatcher;

    @Autowired
    @SneakyThrows
    public StorageService (AppProperties appProperties) {
        val storage = appProperties.getStorage();
        home = storage.getHome();
        if (!Files.exists(home) || !Files.isDirectory(home)) {
            Files.createDirectories(home);
        }

        keyFileExtension = storage.getKeyFileExtension();
        val keyMatcherPattern = new StringBuilder()
                .append("glob:*.")
                .append(keyFileExtension)
                .toString();
        keyMatcher = FileSystems.getDefault().getPathMatcher(keyMatcherPattern);
    }

    @SneakyThrows
    public Optional<String> loadKey () {
        return Files.list(home)
                .map(Path::getFileName)
                .filter(keyMatcher::matches)
                .findFirst()
                .map(this::extractFileName);
    }

    @SneakyThrows
    public void saveKey (String key) {
        Files.list(home)
                .map(Path::getFileName)
                .filter(keyMatcher::matches)
                .forEach(this::delete);

        val keyFileName = new StringBuilder()
                .append(key)
                .append('.')
                .append(keyFileExtension)
                .toString();
        Files.createFile(home.resolve(keyFileName));
    }

    private String extractFileName (Path path) {
        val fileName = path.toString();
        return fileName.substring(0, fileName.lastIndexOf('.'));
    }

    @SneakyThrows
    private void delete (Path path) {
        Files.delete(path);
    }
}
