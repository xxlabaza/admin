/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.user;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Data;
import ru.svetvakula.admin.service.user.role.Role;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Data
public class User implements Serializable {

    private static final long serialVersionUID = -868942790247098889L;

    private String uuid;

    private String email;

    private Date created;

    private List<Role> roles;

    public User () {
    }

    public User (String email) {
        this.email = email;
    }
}
