/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.user;

import feign.Feign;
import feign.Headers;
import feign.Param;
import feign.RequestInterceptor;
import feign.RequestLine;
import feign.codec.Encoder;
import feign.codec.ErrorDecoder;
import feign.jackson.JacksonDecoder;
import java.util.List;
import ru.svetvakula.admin.feign.Page;
import ru.svetvakula.admin.service.user.role.Role;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Headers({
    "Accept:        application/json",
    "Content-Type:  application/json"
})
interface UserApi {

    @RequestLine("POST /api/user")
    User create (User user);

    @RequestLine("GET /api/user")
    Page<User> findAll ();

    @RequestLine("GET /api/user/{uuid}")
    User find (@Param("uuid") String uuid);

    @RequestLine("GET /api/user/{uuid}/role")
    List<Role> getRoles (@Param("uuid") String uuid);

    @RequestLine("PATCH /api/user/{uuid}")
    User patch (@Param("uuid") String uuid, User updates);

    @RequestLine("PUT /api/user/{uuid}/role/{roles_aliases}")
    List<Role> setRoles (@Param("uuid") String uuid, @Param("roles_aliases") String roles);

    @RequestLine("PATCH /api/user/{uuid}/role/{roles_aliases}")
    List<Role> addRoles (@Param("uuid") String uuid, @Param("roles_aliases") String roles);

    @RequestLine("DELETE /api/user/{uuids}")
    void delete (@Param("uuids") String uuids);

    @RequestLine("DELETE /api/user/{uuid}/role/{roles_aliases}")
    List<Role> deleteRoles (@Param("uuid") String uuid, @Param("roles_aliases") String roles);

    static UserApi create (String serverUrl,
                           Encoder encoder,
                           JacksonDecoder jacksonDecoder,
                           List<RequestInterceptor> interceptors,
                           ErrorDecoder errorDecoder
    ) {
        return Feign.builder()
                .encoder(encoder)
                .decoder(jacksonDecoder)
                .requestInterceptors(interceptors)
                .errorDecoder(errorDecoder)
                .target(UserApi.class, serverUrl);
    }
}
