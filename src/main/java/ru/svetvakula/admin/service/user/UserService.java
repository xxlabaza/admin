/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.user;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.svetvakula.admin.feign.Page;
import ru.svetvakula.admin.service.user.role.Role;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Service
public class UserService {

    @Autowired
    private UserApi userApi;

    public User create (String email) {
        return userApi.create(new User(email));
    }

    public Page<User> findAll () {
        return userApi.findAll();
    }

    public User find (String uuid) {
        return userApi.find(uuid);
    }

    public List<Role> getRoles (String uuid) {
        return userApi.getRoles(uuid);
    }

    public User patch (String uuid, User updates) {
        return userApi.patch(uuid, updates);
    }

//    public List<Role> setRoles (String uuid, List<String> roles) {
//        return userApi.setRoles(uuid, roles);
//    }
//
//    public List<Role> addRoles (String uuid, List<String> roles) {
//        return userApi.addRoles(uuid, roles);
//    }
//
//    public void delete (List<String> uuids) {
//        userApi.delete(uuids);
//    }
//
//    public List<Role> deleteRoles (String uuid, List<String> roles) {
//        return userApi.deleteRoles(uuid, roles);
//    }
}
