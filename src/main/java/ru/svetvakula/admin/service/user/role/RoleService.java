/*
 * Copyright 2016 Artem Labazin - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package ru.svetvakula.admin.service.user.role;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Artem Labazin <xxlabaza@gmail.com>
 * @since 24.04.2016
 */
@Service
public class RoleService {

    @Autowired
    private RoleApi roleApi;

    public List<Role> getAll () {
        return roleApi.getAll();
    }
}
